<a href="https://github.com/RickDB/PlexAniSync">
  <img src="./.gitlab/assets/logo.png" width="90"/>
</a>

<h1>Preefix's <a href="https://github.com/RickDB/PlexAniSync">PlexAniSync</a> Custom Mappings</h1>

Custom mappings intended for use with the [PlexAniSync](https://github.com/RickDB/PlexAniSync) project.

### But Why?

Because AniDB and AniList don't like working with each other. The former likes to use `O` instead of `wo` like AniList, causing some mismatches.

### Contributing

Pull requests are welcome. For questions and major changes, please open an issue first to discuss what you would like to change.